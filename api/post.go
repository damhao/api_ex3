package api

import (
	"net/http"
)

type PostInterface interface {
	GetPostByID(w http.ResponseWriter, h *http.Request)
}
