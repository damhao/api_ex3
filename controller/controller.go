package controller

import (
	"API/models"
	"API/response"
	"API/utils"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"time"

	"API/types"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis/v8"
)

var (
	requestChannel  = "request"
	responseChannel = "response"
)

type PostService struct {
	// api.PostInterface
	FilePath    string
	PostModel   *models.PostModel
	RedisClient *redis.Client
}

func InitPostService(postModel *models.PostModel, redisClient *redis.Client, filePath string) *PostService {
	return &PostService{
		PostModel:   postModel,
		FilePath:    filePath,
		RedisClient: redisClient,
	}
}

func (p *PostService) GetPostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()

	paramStr := h.URL.Query().Get("id")

	id, err := strconv.Atoi(paramStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// post, err := p.PostModel.GetPostByID(id)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	// fmt.Println(post)

	ctx := context.Background()
	var param = make(map[string]string)
	param["id"] = fmt.Sprint(id)

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	var request = types.RedisRequest{
		Name:   "getPostByID",
		Params: param,
		Id:     index,
	}
	requestJSON, err := json.Marshal(&request)

	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}

	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var post types.PostTable

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		var result types.RedisResponse
		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {

			err := json.Unmarshal([]byte(result.Data), &post)
			if err != nil {
				fmt.Println("Error", err)
				response.ResponseError(w, 500, fmt.Errorf("error when unmarshal posts  : %v", err))
				return
			}

			break

		} else {
			continue
		}

	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByID\nINPUT : id = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", id, post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponsePost(w, 200, post)

}
func (p *PostService) GetAllPosts(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	pageStr := h.URL.Query().Get("page")
	page, err := strconv.Atoi(pageStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	limitStr := h.URL.Query().Get("limit")
	limit, err := strconv.Atoi(limitStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}
	ctx := context.Background()

	var param = make(map[string]string)
	param["page"] = fmt.Sprint(page)
	param["limit"] = fmt.Sprint(limit)

	id, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	var request = types.RedisRequest{
		Name:   "getAllPosts",
		Params: param,
		Id:     id,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}
	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var posts []types.PostTable

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		var result types.RedisResponse
		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == id {

			err := json.Unmarshal([]byte(result.Data), &posts)
			if err != nil {
				fmt.Println("Error", err)
				response.ResponseError(w, 500, fmt.Errorf("error when unmarshal posts  : %v", err))
				return
			}

			break

		} else {
			continue
		}

	}
	// haskell , ada
	// posts, err := p.PostModel.GetAllPosts(page, limit)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetAllPosts\nINPUT : page = %d, limit = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", page, limit, posts, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, posts)
	//w.Write()

}
func (p *PostService) DeletePostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	paramStr := h.URL.Query().Get("id")

	id, err := strconv.Atoi(paramStr)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// err = p.PostModel.DeletePostByID(id)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	ctx := context.Background()
	var param = make(map[string]string)

	param["id"] = fmt.Sprint(id)

	index, err := p.RedisClient.Get(ctx, "id").Int()

	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	var request = types.RedisRequest{
		Name:   "deletePostByID",
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)

	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}

	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var result types.RedisResponse

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {
			break

		} else {
			continue
		}
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : DeletePostByID\nINPUT : id = %d\nOUTPUT :\nCREATED_TIME : %v\n\n", "DELETE", id, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}
	response.ResponseWithJson(w, http.StatusOK, result.Data)
}

func (p *PostService) CreatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// err = p.PostModel.CreatePost(post)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)

	param["post"] = string(body)

	var request = types.RedisRequest{
		Name:   "createPost",
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}
	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var res string

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		var result types.RedisResponse
		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {
			res = result.Data
			break
		}

	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : CreatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "POST", post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error :", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, res)
}
func (p *PostService) UpdatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// err = p.PostModel.UpdatePost(post)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)

	param["post"] = string(body)

	var request = types.RedisRequest{
		Name:   "updatePost",
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}
	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var res string

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		var result types.RedisResponse
		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {
			res = result.Data
			break
		}

	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : UpdatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "PUT", post, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, res)

}
func (p *PostService) GetPostByTitle(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	var title string

	err = json.Unmarshal(body, &title)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// var post types.PostTable
	// err = json.Unmarshal(body, &post)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	// posts, err := p.PostModel.GetPostByTitle(title)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }

	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)
	param["post"] = string(body)

	var request = types.RedisRequest{
		Name:   "getPostsByTitle",
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}

	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var result types.RedisResponse
	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {
			break

		} else {
			continue
		}
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : DeletePostByID\nINPUT : id = %d\nOUTPUT :\nCREATED_TIME : %v\n\n", "DELETE", title, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, result.Data)

}

func (p *PostService) GetPostByTopic(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	var topic string

	err = json.Unmarshal(body, &topic)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, err)
		return
	}

	// posts, err := p.PostModel.GetPostByTopic(topic)
	// if err != nil {
	// 	fmt.Println("Error", err)
	// 	response.ResponseError(w, 500, err)
	// 	return
	// }
	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)
	param["post"] = string(body)

	var request = types.RedisRequest{
		Name:   "getPostsByTopic",
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, fmt.Errorf("can't marshal request : %v", err))
		return
	}

	p.RedisClient.Publish(ctx, requestChannel, requestJSON)

	sub := p.RedisClient.Subscribe(ctx, responseChannel)

	var posts []types.PostTable

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when receiving message : %v", err))
			return
		}

		var result types.RedisResponse
		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			fmt.Println("Error", err)
			response.ResponseError(w, 500, fmt.Errorf("error when unmarshal message : %v", err))
			return
		}

		if result.Id == index {

			err := json.Unmarshal([]byte(result.Data), &posts)
			if err != nil {
				fmt.Println("Error", err)
				response.ResponseError(w, 500, fmt.Errorf("error when unmarshal posts  : %v", err))
				return
			}
			break

		} else {
			continue
		}

	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByTopic\nINPUT : topic = %s\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", topic, now), p.FilePath)
	if err != nil {
		fmt.Println("Error", err)
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, posts)

}
