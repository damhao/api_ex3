package service

import (
	"API/database"
	"API/models"
	"API/types"
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

var (
	requestChannel  = "request"
	responseChannel = "response"
	numOfWorkers    = 10
)

func TaskQueue() {

	fmt.Println("Task queue is running !")
	ctx := context.Background()

	rdb := InitRedisClient()

	db := database.InitConnection()

	p := models.InitPostModel(db)

	pubsub := rdb.Subscribe(ctx, requestChannel)

	var requestChan = make(chan types.RedisRequest)

	defer close(requestChan)

	for i := 1; i <= numOfWorkers; i++ {
		go ProcessTask(ctx, rdb, p, requestChan, i)
	}

	for {
		message, err := pubsub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Printf("Error when receiving message : %v\n", err)
		}

		fmt.Printf("received %+v from TASK QUEUE\n", message.Payload)

		var request types.RedisRequest
		err = json.Unmarshal([]byte(message.Payload), &request)
		if err != nil {
			fmt.Printf("Can't unmarshal message : %v\n", err)
		}

		requestChan <- request

		time.Sleep(time.Millisecond * 100)
	}
}

func ProcessTask(ctx context.Context, rdb *redis.Client, p *models.PostModel, requestChan chan types.RedisRequest, workerId int) {

	for request := range requestChan {
		fmt.Printf("worker %d received %+v\n", workerId, request)

		switch request.Name {

		case "getAllPosts":
			page, err := strconv.Atoi(request.Params["page"])
			if err != nil {
				fmt.Println(err)
			}
			limit, err := strconv.Atoi(request.Params["limit"])
			if err != nil {
				fmt.Println(err)
			}
			posts, err := p.GetAllPosts(page, limit)
			if err != nil {
				fmt.Println(err)
			}

			postJSON, err := json.Marshal(&posts)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			var response = types.RedisResponse{
				Id:   request.Id,
				Data: string(postJSON),
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

		case "getPostByID":
			id, err := strconv.Atoi(request.Params["id"])
			if err != nil {
				fmt.Println(err)
			}
			post, err := p.GetPostByID(id)
			if err != nil {
				fmt.Println(err)
			}
			postJSON, err := json.Marshal(&post)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			var response = types.RedisResponse{
				Id:   request.Id,
				Data: string(postJSON),
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		case "createPost":
			postJSON := request.Params["post"]

			var post types.PostTable

			err := json.Unmarshal([]byte(postJSON), &post)
			if err != nil {
				fmt.Println(err)
			}

			var response types.RedisResponse
			err = p.CreatePost(post)
			if err != nil {
				fmt.Println(err)
				response = types.RedisResponse{
					Id:   request.Id,
					Data: "Cant create post",
				}
			}

			response = types.RedisResponse{
				Id:   request.Id,
				Data: "Created successfully",
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		case "deletePostByID":
			id, err := strconv.Atoi(request.Params["id"])
			if err != nil {
				fmt.Println(err)
			}
			var response types.RedisResponse

			err = p.DeletePostByID(id)
			if err != nil {
				fmt.Println(err)
				response = types.RedisResponse{
					Id:   request.Id,
					Data: "failed to delete the article",
				}
			}

			response = types.RedisResponse{
				Id:   request.Id,
				Data: "Delete successfully",
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		case "updatePost":
			postJSON := request.Params["post"]

			var post types.PostTable

			err := json.Unmarshal([]byte(postJSON), &post)
			if err != nil {
				fmt.Println(err)
			}

			var response types.RedisResponse
			err = p.UpdatePost(post)
			if err != nil {
				fmt.Println(err)
				response = types.RedisResponse{
					Id:   request.Id,
					Data: "Cant update post",
				}
			}

			response = types.RedisResponse{
				Id:   request.Id,
				Data: "Update successfully",
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		case "getPostsByTitle":
			post, err := p.GetPostByTitle(request.Params["title"])
			if err != nil {
				fmt.Println(err)
			}
			postJSON, err := json.Marshal(&post)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			var response = types.RedisResponse{
				Id:   request.Id,
				Data: string(postJSON),
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		case "getPostsByTopic":
			post, err := p.GetPostByTopic(request.Params["topic"])
			if err != nil {
				fmt.Println(err)
			}
			postJSON, err := json.Marshal(&post)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			var response = types.RedisResponse{
				Id:   request.Id,
				Data: string(postJSON),
			}

			responseJSON, err := json.Marshal(&response)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, responseChannel, err.Error())
			}

			pub := rdb.Publish(ctx, responseChannel, string(responseJSON))
			if pub.Err() != nil {
				rdb.Publish(ctx, responseChannel, fmt.Errorf("error when sending response"))
			}

			fmt.Println(response)

		}
		time.Sleep(time.Millisecond * 100)
	}
}

func InitRedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}
